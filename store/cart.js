import { defineStore } from "pinia";

export const useCartStore = defineStore('cart', {
    state: () => ({
        cart: []
    }),
    getters: {
        itemsInCart: state => state.cart 
    },
    actions: {
        addItemToCart(item) {
            const inCart = this.cart.find(p => p.id === item.id);

            const { id,image, price, title } = item;
        
            if (!inCart) {
                this.cart.push({
                    id,
                    image,
                    price,
                    title
                });
            }
        }
    }
})