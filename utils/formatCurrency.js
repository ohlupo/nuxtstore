export const formatCurrency = value => {
    let options = {
        style: 'currency',
        currency: 'USD'
    }

    return new Intl.NumberFormat('en-us', options).format(value);
}